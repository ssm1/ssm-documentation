# Cloning  InfluxDB

You might want to move data from your local InfluxDB instance into DBOD InfluxDB or another instance where you cannot use influxd backup/restore because you don't have access to the server. To do that, you need to first extract your data from InfluxDB using line protocol, and then you can import it into the other InfluxDB.

## First method. Database small

### Exporting data from InfluxDB

First, export the data from your InfluxDB instance using the influx_inspect utility with the export command and -compress option, like below:

```bash
influx_inspect export -datadir /var/lib/influxdb/data \
-waldir /var/lib/influxdb/wal -out ./export
```

!!!note
    The default location for the export file is in $HOME/.influxdb/data. To change the location for the export file, add the -out <export_dir> option.
    By default, export exports all databases from your instance. To import specific databases, use the -database <db_name> option.
    Click [HERE](<https://docs.influxdata.com/influxdb/v1.8/tools/influx_inspect/#export>) for a complete list of available export options.

!!!warning
    The dump of a big database can take hours and get lots of memory and resource to the server. Do it the night or when you don't have a heavy work in your server.
    I recomend always using tmux to avoid interruptions.

### Import data

Wait some minutes to have enough data in the "export" file and open a new terminal.
Run:

```bash

cat ./export | influx -password <pass> -username admin \
-host <the_host> --port 8086 --ssl --import --path /dev/stdin

```

What we are doing is passing the file export that is being filled with the data being exported by the other terminal.
the path is the stdin were we are sending the `cat` command.

Et voila...

You can wait also until everything is finished in the export but is not necessary, in general is going to be quicker the export in the local host that the sending data over ssl, remotly, etc.

!!! Warning
    If your database is too big you risk to make a file so big that block the server! I did in the first try, so I must use crunchs of data with the next procedure

## Second method. Big database

The second solution is making crunchs of data by time and compress the output. Later, use the same command from the import that before:

Export:

```bash
influx_inspect export -datadir /var/lib/influxdb/data -start "2022-12-01T00:00:00Z" \
-end "2022-12-15T00:00:00Z" -waldir /var/lib/influxdb/wal -database <database> -compress -out ./export
```

The export is faster if you don't do compress. For import is the same speed.

Import:

```bash
influx -password <pass> -username admin \
-host <the_host> -port 8086 -ssl -compressed -import -path ./export
```

As reference, in my big server (24 vCPU, 48GB RAM) makes something like 180000 points per second for importing the data. Around 15 days data in 5 minutes.

You can do a batch with an interactive loop in a bash script to make several months in one shot.

This is the final script I used. I should have made a nice for-loop but I was too lazy ;-)

```bash
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-01-01T00:00:00Z" -end "2022-02-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-02-01T00:00:00Z" -end "2022-03-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-03-01T00:00:00Z" -end "2022-04-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-04-01T00:00:00Z" -end "2022-05-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-05-01T00:00:00Z" -end "2022-06-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-06-01T00:00:00Z" -end "2022-07-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-07-01T00:00:00Z" -end "2022-08-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-08-01T00:00:00Z" -end "2022-09-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-09-01T00:00:00Z" -end "2022-10-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-10-01T00:00:00Z" -end "2022-11-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-11-01T00:00:00Z" -end "2022-12-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -start "2022-12-01T00:00:00Z" -end "2023-01-01T00:00:00Z" -database telegraf -out ./export
influx -password <the_pass> -username admin -host <the_host> -port <the_port> -ssl  -import -path ./export
```
