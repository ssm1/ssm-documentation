# Snapshots for rollback & rebuild
  !!! Only works with VM without volumes !!!
  
   * Create a snapshot file of the virtual machine into glance for the reference configuration at the start of the test. This snapshot can then be used later for the rebuild reinstallation.
   * Use `openstack server rebuild` to roll back to the same state with same IP and hostname. This is much faster than deleting the old server and creating a new one with the same name (of the order of 2-3 minutes)

**Note:** During the time it takes to do the snapshot, the machine can become unresponsive. It is recommended to resync the VM clocks using the NTP daemon or running ntpdate after the snapshot has completed.

```
$ openstack server image create --name my-snapshot --wait my-vm
Finished
$ openstack image show --fit-width my-snapshot
```

Using this snapshot, the VM can be rolled back to the previous state with a server rebuild.

```
$ openstack server rebuild --image my-snapshot my-vm
```
