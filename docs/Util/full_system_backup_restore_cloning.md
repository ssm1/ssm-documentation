# Full System Backup Restore Clone

This section is about using rsync to transfer a copy of the entire / tree, excluding a few selected folders. This approach is considered to be better than disk cloning with dd since it allows for a different size, partition table and filesystem to be used, and better than copying with cp -a as well, because it allows greater control over file permissions, attributes, Access Control Lists and extended attributes.

rsync will work even while the system is running, but files changed during the transfer may or may not be transferred, which can cause undefined behavior of some programs using the transferred files.

This approach works well for migrating an existing installation to a new hard drive or SSD.

Run the following command as root to make sure that rsync can access all system files and preserve the ownership:

```bash
rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / /path/to/backup/folder
```

By using the -aAX set of options, the files are transferred in archive mode which ensures that symbolic links, devices, permissions, ownerships, modification times, ACLs, and extended attributes are preserved, assuming that the target file system supports the feature.

The --exclude option causes files that match the given patterns to be excluded. The contents of /dev, /proc, /sys, /tmp, and /run are excluded in the above command, because they are populated at boot, although the folders themselves are not created. /lost+found is filesystem-specific. The command above depends on brace expansion available in both the bash and zsh shells. When using a different shell, --exclude patterns should be repeated manually. Quoting the exclude patterns will avoid expansion by the shell, which is necessary, for example, when backing up over SSH. Ending the excluded paths with * ensures that the directories themselves are created if they do not already exist.

If you plan on backing up your system somewhere other than /mnt or /media, do not forget to add it to the list of exclude patterns to avoid an infinite loop.
If there are any bind mounts in the system, they should be excluded as well so that the bind mounted contents is copied only once.
If you use a swap file, make sure to exclude it as well.
Consider if you want to backup the /home/ folder. If it contains your data it might be considerably larger than the system. Otherwise consider excluding unimportant subdirectories such as `/home/*/.thumbnails/*`, `/home/*/.cache/mozilla/*`, `/home/*/.cache/chromium/*`, and `/home/*/.local/share/Trash/*`, depending on software installed on the system. If GVFS is installed, `/home/*/.gvfs` must be excluded to prevent rsync errors.
You may want to include additional rsync options, such as the following. See rsync(1) for the full list.

If you use many hard links, consider adding the -H option, which is turned off by default due to its memory expense; however, it should be no problem on most modern machines. Many hard links reside under the /usr/ directory.

You may want to add rsync’s --delete option if you are running this multiple times to the same backup folder. In this case make sure that the source path does not end with /*, or this option will only have effect on the files inside the subdirectories of the source directory, but it will have no effect on the files residing directly inside the source directory.
If you use any sparse files, such as virtual disks, Docker images and similar, you should add the -S option.
The --numeric-ids option will disable mapping of user and group names; instead, numeric group and user IDs will be transfered. This is useful when backing up over SSH or when using a live system to backup different system disk.
Choosing --info=progress2 option instead of -v will show the overall progress info and transfer speed instead of the list of files being transferred.
Full System Restore
If you wish to restore a backup, use the same rsync command that was executed but with the source and destination reversed.

File System Cloning
rsync provides a way to do a copy of all data in a file system while preserving as much information as possible, including the file system metadata. It is a procedure of data cloning on a file system level where source and destination file systems do not need to be of the same type. It can be used for backing up, file system migration or data recovery.

rsync’s archive mode comes close to being fit for the job, but it does not back up the special file system metadata such as access control lists, extended attributes or sparse file properties. For successful cloning at the file system level, some additional options need to be provided:

`rsync -qaHAXS SOURCE_DIR DESTINATION_DIR`

And their meaning is (from the manpage):

-H, --hard-links      preserve hard links
-A, --acls            preserve ACLs (implies -p)
-X, --xattrs          preserve extended attributes
-S, --sparse          handle sparse files efficiently
Produced copy can be simply reread and checked (for example after a data recovery attempt) at the file system level with diff‘s recursive option:

diff -r SOURCE_DIR DESTINATION_DIR
It is possible to do a successful file system migration by using rsync as described in this article and updating the fstab and bootloader. This essentially provides a way to convert any root file system to another one.

# Cloning

The script to clone a host is:

```bash
if [ -z ${HOSTNAME+x} ]; then
   echo "HOSTNAME is unset. I'm not going to do anything...";
else

sudo rsync -aXASHPr \
--rsync-path="sudo /usr/bin/rsync" \
--force \
--delete \
--progress \
-e ssh \
--exclude=/etc/fstab \
--exclude=/etc/sysconfig/network-scripts \
--exclude=/proc \
--exclude=/tmp \
--exclude=/sys \
--exclude=/dev \
--exclude=/mnt \
--exclude=/boot  \
--exclude=/run \
--exclude=/tmp \
--exclude=/afs \
--exclude=/lost+found \
--exclude=/backup/* \
/ root@ssm2.cern.ch:/home/kiosk/$HOSTNAME/

fi
```

# Restoring the host

Really tricky but the selinux system has to be stopped to allow the restore. Steps to be done for the clone:

Install CENTOS8 minimum from the CERN repository with PXE boot from network
Edit the /etc/selinux/config. Should be like that:

```bash
# This file controls the state of SELinux on the system
# SELINUX= can take one of these three values
# enforcing - SELinux security policy is enforced
# permissive - SELinux prints warnings instead of enforcing
# disabled - No SELinux policy is loaded
SELINUX=disabled
# SELINUXTYPE= can take one of three two values
# targeted - Targeted processes are protected
# minimum - Modification of targeted policy. Only selected processes are protected.
# mls - Multi Level Security protection
SELINUXTYPE=targeted
```

Reboot the machine and run the next script:

```bash
HOST_TO_BE_RESTORED=hostname

# To avoid problems with selinux

/usr/sbin/setenforce 0

ThisHost=`hostname | awk -F. '{print $1}'`

sudo rsync -aXASHPr \
--rsync-path="sudo /usr/bin/rsync" \
--force \
--delete \
--progress \
-e ssh \
--exclude=/proc \
--exclude=/tmp \
--exclude=/sys \
--exclude=/dev \
--exclude=/mnt \
--exclude=/run \
--exclude=/tmp \
--exclude=/afs \
--exclude=/boot \
--exclude=/etc/fstab \
--exclude=/etc/mtab \
--exclude=/etc/grub.cfg \
--exclude=/etc/lvm \
--exclude=/lost+found \
--exclude=/srv \
--exclude=/media \
--exclude=/root/backups/mysql \
root@ssm2.cern.ch:/home/$HOST_TO_BE_RESTORED/* /

```

Modify the /etc/hostname in the target host with the new hostname assigned.
Reboot and enjoy.
