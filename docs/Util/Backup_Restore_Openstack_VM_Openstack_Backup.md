
All is based in ssm-alma8 server
All the doc for openstack in: https://clouddocs.web.cern.ch

In ssm1 source the openstack environment:
`root@ssm1:~ >  . openstack.env`

Test openstack with e.g:
`root@ssm1:~ > openstack image list`

# Backup:
```
openstack volume backup create ssm-alma8 --name backup-ssm-alma8 --description 'Backup of ssm-alma8' --force
```

Check the status of a backup with:

`openstack volume backup show backup-ssm-alma8`

# Restore
Use the a volume from the backup found with `openstack volume backup list`.

Once identified, it is possible to create a new volume from the backup:

`openstack volume backup restore backup-ssm-alma8`


run: 
```
openstack server list
openstack volume list
openstack key list
``` 
to find the flavor, volume to use and the key to use for 
In this moment the old VM must be erased.
Volume cannot be detached or switched. VM must to be erase and recreated with the backup volume.



Create the new VM:

```
openstack server create --flavor m2.large --key-name root-ssm1 --property cern-checkdns=false --property cern-waitdns=false --property landb-ipv6ready=false --volume 331ae501-3da0-4af2-adc2-98b10041fbc6 ssm-alma8
```



Reboot.

