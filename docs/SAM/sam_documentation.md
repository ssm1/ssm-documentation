# SAM

## Introduction

SAM stands for Simple Access Messages and is a sub-system of SSM (Safety Systems Monitoring) project.
The aim of the project is show messages in the access points with useful information for the users entering in the accelerators, experiments or restricted areas.

## Description

The kind of information shown is general safety rules, special indications for the areas or telephone numbers to be called in case of urgency. The system can also shown basic images, text colors, big text, etc. The information is rotating between different pages to show more information to the users. Generally there are 2 main screens in the slideshow, one specific for the access point or experimental area and other more general for the accelerator globally. Examples of screens:

![Alt text](img/examples_screens_sam.png)

## Hardware

The typical SAM post is a screen attached to a small device connected to the SAM server by an internet connection. At present 2 main devices are used: The Raspberry Pi Model 3b and NUC Intel. The RPi are used for unattended screens and the NUC are doing others tasks related with RP, Impact, ADAMs, etc.

![Alt text](img/hardware_diagram_sam.png)
There are 35 Raspberry Pi 3 and 34 NUC Intel MiniPC. In Total 69 Hosts.

### List of hardware SAM

![Alt text](img/list_hosts_sam_1.png)
![Alt text](img/list_hosts_sam_2.png)
![Alt text](img/list_hosts_sam_3.png)

## Software

The SAM software consist in a CMS (Content Manager System) open-source simple. The footprint is really small and make pages in pure HTML without javascript or complex templates.
The server is running Apache web server for serving the pages. The system deliver around 700,000 HTML pages per day and around 80 clients.
A complete report can be found here: <http://ssm.cern.ch/report.html>

![Alt text](img/unique_visitors_sam.png)

There are 2 main protocol in order to access to the pages in the SAM Web Server. One is provided by a PHP Server who check the machine requesting the pages and depending of its IP or DNS name delivers one page or other.
The second method is direct request from the client browser of the page. Mainly the RPi use the PHP Who Are You ? Protocol and the NUC make direct requests to the server.
In each machine is working a slideshow of 2 pages. One for the General Accelerator and a second one depending from where the machine is.

The pages are edited by the software GetSimple CMS (<https://get-simple.info/>) installed in the SAM Web server that is inside the SSM General Software packages. GetSimple is an XML based, stand-alone, fully independent and lite Content Management System.
Example editing a page with GetSimple:

![Alt text](img/get_simple_sam.png)

In the RPi and NUC are acting as kiosk without any intervention to show the pages. In the RPi there is a chromium browser really simple and in the NUC’s a Firefox browser since the machine has more capabilities. Also in the NUC systems the host is used to show important information about Impact and ADAMs information.

Here you can find the software diagram:

![Alt text](img/software_diagram_sam.png)
