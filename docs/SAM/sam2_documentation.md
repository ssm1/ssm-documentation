# SAM 2.0

## Introduction

----
SAM stands for Simple Access Messages and is a sub-system of SSM (Safety Systems Monitoring) project.
The aim of the project is show messages in the access points with useful information for the users entering in the accelerators, experiments or restricted areas.

In the last years we have been confronted to many problems with the hardware used in the SAM service, the Raspberry Pi devices. More and more is difficult to find hardware and we have to find other solutions.

In the other hand, pointing to the main SSM server for the pages is dangerous and makes the updates and modifications really difficult.

## Proposal

----

* Find another hardware to replace the RPi's
* Deploy a new server for the service in the Openstack service at CERN with more functionalities that now, specially the possibility of using big images, videos, etc.

## Requirements

----

* The device (Windows, Linux, Mac) must to start from power-off to a standard web browser (tested with Firefox and Chrome) in full screen and point to the next URL's:
  * For LHC: <http://ssm-sam.cern.ch/slideshow.html>
  * For SPS: <http://ssm-sam.cern.ch/slideshow-sps.html>
  * For PS:  <http://ssm-sam.cern.ch/slideshow-ps.html>

* The host must to avoid having screen-savers, hardware sleeps, mouse pointers, etc. when showing the URL.
* The hostname must to be included in the configuration file in the SAM server in order to deliver the correct slideshow to the users. This is done by the SAM administrator.
* Follow the instructions to be monitored by SSM: <https://ssm.docs.cern.ch/SSM/Monitored_by_Zabbix/>

## Prototypes

----

### Odroid Prototype

----

#### Hardware

----
The first prototype is based in a ODROID N2+ 4GB  with a cost of 120 CHF.

The complete specification is here: <https://wiki.odroid.com/odroid-n2/hardware>

We request a eMMC Odroid Memory but we don't have received yet. For the moment, the test will be done with SD Card of 32 GB.

![Alt text](img/odroid.jpg)

We didn't test yet in a big screen but once the test in the lab done, we will install in an access point.

#### Software

----
The software is based in a Ubuntu 22.04 with a MATE Desktop: <https://wiki.odroid.com/odroid-n2/os_images/ubuntu>

The OS has been modified to avoid the screensavers and the power off after a while.
Also, the browser is started automatically pointing to the correct new server page.

The server has been installed in a VM in openstack: <http://ssm-sam.cern.ch>

### Test

----
We tested the prototype with the next objectives:

* Must restart with the page running and with the screen "ON"
* Must to be operational 24/24 7/7 without going to "black screen"
* Must to have no or small memory drains after days running (a problem with the old RPi)
* The slideshow must to show big images or videos without delays or retentions.

After 2 weeks running, the above specifications have been verified. After a week we included a big 10 seconds video (5 MB) in the slideshow.

The test page can be found here:
<http://ssm-sam.cern.ch/test_tono>

Next, we can see the performances obtained here:

<https://ssm-zabbix.cern.ch/zabbix.php?action=search&search=SSM-ODROID>

![Alt text](img/sam2_performance.png)

We can observe in the graphics that:

* The CPU usage is big with the video but the maximum used is 17.67% -> Good
* THere is not memory leak. The max memory used has been 0.77 GB -> Good
* No swap memory was necessary -> Good

### Conclusion

----
The Odroid prototype is a valid hardware to replace the RPi. Still, we need to test the eMMC memory before validate it completely.
