# Status for Long Term Access Data

## Subsystems

<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/3sgJ5wWd/mysql.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/DW528iVL/oracle.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/L_qik9GN/ssm-grafana.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/r10AXcMw/susi.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/uyJ4llVa/zora.svg">

## Overall Status

<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/K9W73CTC.svg">
