# Access

The data is [HERE](<https://ssm-grafana.cern.ch>){target="_blank"}. You must to have a CERN login valid and be approved before access the data:

In order to give the long term data, some preparation work must to be done by SSM.
The data is shown in Grafana subsystem after some treatment the process is described here:

[ORACLE to MySQL Bridge](oracle_to_mysql.md)

All the process is supervised by HealthChecks in [SSM-HC](<https://ssm-hc.cern.ch>){target="_blank"}
