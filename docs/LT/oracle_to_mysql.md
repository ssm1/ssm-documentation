# Oracle to MySQL Bridge

The aim is doing a bridge for data stored in ORACLE to be saved in a MySQL DB that will be used by Grafana dashboards to show data for the access.

# Structure

## Parameters

Before everything the tables must be created in the MySQL SSM or DBOD instances. The tables must have at least 2 columns, "time" and "value".

## Tables in the database 'people'

### distinct_persons_per_accelerator

```sql
CREATE TABLE `distinct_persons_per_accelerator` (
  `time` date DEFAULT NULL,
  `accelerator` varchar(45) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### distinct_persons_per_orgunit

```sql
CREATE TABLE `distinct_persons_per_orgunit` (
  `time` date DEFAULT NULL,
  `accelerator` varchar(10) DEFAULT NULL,
  `class` varchar(25) DEFAULT NULL,
  `department` varchar(10) DEFAULT NULL,
  `dep_group` varchar(20) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### distinct_persons_per_zone

```sql
CREATE TABLE `distinct_persons_per_zone` (
  `time` datetime DEFAULT NULL,
  `accelerator` varchar(25) DEFAULT NULL,
  `site` varchar(25) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### movements_pad

```sql
CREATE TABLE `movements_pad` (
  `time` date DEFAULT NULL,
  `accelerator` varchar(25) DEFAULT NULL,
  `site` varchar(25) DEFAULT NULL,
  `direction` varchar(25) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### simultaneous_persons_per_accelerator`

```sql
CREATE TABLE `simultaneous_persons_per_accelerator` (
  `time` date DEFAULT NULL,
  `accelerator` varchar(45) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### simultaneous_persons_per_zone

```sql
CREATE TABLE `simultaneous_persons_per_zone` (
  `time` date DEFAULT NULL,
  `accelerator` varchar(10) DEFAULT NULL,
  `site` varchar(10) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### susi_distinct_persons_per_orgunit

```sql
CREATE TABLE `susi_distinct_persons_per_orgunit` (
  `time` date DEFAULT NULL,
  `zone` varchar(10) DEFAULT NULL,
  `class` varchar(25) DEFAULT NULL,
  `department` varchar(10) DEFAULT NULL,
  `dep_group` varchar(20) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### susi_simultaneous_persons_cern`

```sql
CREATE TABLE `susi_simultaneous_persons_cern` (
  `time` date DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### susi_simultaneous_persons_per_zone

```sql
CREATE TABLE `susi_simultaneous_persons_per_zone` (
  `time` date DEFAULT NULL,
  `zone` varchar(45) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### susi_transactions_per_acp

```sql
CREATE TABLE `susi_transactions_per_acp` (
  `time` date DEFAULT NULL,
  `acp` varchar(45) DEFAULT NULL,
  `value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

## Retrieve and store the data

The data is pulled by a bash script in the SSM Server and saved as a temporal CSV file.
In the same script, the MySQL table in the db is erased and recreated with the new data. This is quicker than insert values in tables that sometimes have hundred of thousand values.

Also because the data coming from the ORACLE DB is generated every night and sometimes there are modifications not only in the day but days before (checked with Pedro)

The scripts are running every day around 6h and are 10 scripts, one per table.
The whole process is checked by Healthchecks system in ssm2.cern.ch. If some script arise an error the manager gets a email, telegram, etc.  This system is also used to measure the performance of the system and check bottle-necks.

An example of the script is:

```bash
#!/bin/bash

health=64369490-4e90-46b7-a7af-da55e1d940bf

curl -fsS -m 10 --retry 5 -o /dev/null  http://ssm2.cern.ch/ping/$health/start

minimum_lines=5000
pass_oracle=`cat /opt/oracle/oracle_susi.conf`
conf_mysql=/opt/oracle/mysql.conf
conf_mysql_2=/opt/oracle/dbod.conf
file_csv=/opt/oracle/susi_distinct_persons_per_orgunit.csv
rm -rf $file_csv; touch $file_csv
mysql_table="susi_distinct_persons_per_orgunit"

output=`/usr/bin/sqlplus -S $pass_oracle <<EOF
SET ECHO OFF
SET FEEDBACK OFF
SET COLSEP ,
SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 60
SET TRIM ON
SET TERMOUT OFF

spool $file_csv

SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ',' || WHICH || ',' || CLASS || ',' || DEPARTMENT || ',' || "GROUP" || ',' || "COUNT"
  FROM ATOL_S.V_PRS_PER_DAY_ZONE_CLASS_GROUP
ORDER BY WHEN
;

spool off
exit;
EOF`

# Count the lines in the csv file to check if it is too small.

actual_lines=$(wc -l <"$file_csv")

if [ $actual_lines -ge $minimum_lines ]; then

  # The file is bigger than minimum_lines. In principle, good.
  # First, erase the data from the DB

  /usr/bin/mysql --defaults-extra-file=$conf_mysql people -e "set global local_infile=true"
  /usr/bin/mysql --defaults-extra-file=$conf_mysql people -e "delete from $mysql_table"

  # Import data to mysql from csv

  /usr/bin/mysql --defaults-extra-file=$conf_mysql --local-infile people -e "LOAD DATA LOCAL INFILE '${file_csv}'  INTO TABLE ${mysql_table}  FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"

  # DBOD at CERN
  # Change this value. If you do not, there is an error when trying to insert data from the csv file

  /usr/bin/mysql --defaults-extra-file=$conf_mysql_2 people -e "set global local_infile=true"

  # First, erase the data from the DB

  /usr/bin/mysql --defaults-extra-file=$conf_mysql_2 people -e "delete from $mysql_table"

  # Import data to mysql from csv

  /usr/bin/mysql --defaults-extra-file=$conf_mysql_2 --local-infile people -e "LOAD DATA LOCAL INFILE '${file_csv}'  INTO TABLE ${mysql_table}  FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"

  # Report to SSM2 Healthchecks

  curl -fsS -m 10 --retry 5 -o /dev/null http://ssm2.cern.ch/ping/$health/$?

else

  # The file is empty or too small. Do nothing and arise the error
  echo "Error in file: $file_csv Probably a connection error"
  echo $output
  curl -fsS -m 10 --retry 5 --data-raw "$output" http://ssm2.cern.ch/ping/$health/fail

fi
```

## Requests to ORACLE

### distinct_persons_per_accelerator

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || COUNT(DISTINCT(WHICH_2))
FROM ATOL_Z.T_ONSITE_STATISTICS
WHERE WHAT='PERSON_ON_DAY'
GROUP BY WHEN, WHICH
Order by WHEN, WHICH
;
```

### distinct_persons_per_orgunit

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ',' || WHICH || ',' || CLASS || ',' || DEPARTMENT || ',' || "GROUP" || ',' || "COUNT"
  FROM ATOL_Z.V_PRS_PER_DAY_ACC_CLASS_GROUP
ORDER BY WHEN
;
```

### distinct_persons_per_zone

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || WHICH_2 || ',' || HOW_MANY
  FROM ATOL_Z.T_ONSITE_STATISTICS
  WHERE WHAT='DAILY_UNQ_PRS_PER_ZONE'
ORDER BY WHEN, WHICH, WHICH_2;
```

### movements_pad

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || WHICH_2 || ',' || WHICH_3 || ',' || HOW_MANY
  FROM ATOL_Z.T_ONSITE_STATISTICS
  WHERE WHAT='DAILY_MOVS_PER_PAD'
ORDER BY WHEN, WHICH, WHICH_2, WHICH_3;
```

### simultaneous_persons_per_accelerator`

```sql
WITH SUMS AS
(
SELECT TRUNC(WHEN) WHEN, WHICH, SUM(HOW_MANY) TOTAL
FROM ATOL_Z.T_ONSITE_STATISTICS
WHERE WHAT='BY_ZONE'
GROUP BY WHEN, WHICH
)
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || MAX(TOTAL)
  FROM SUMS GROUP BY WHEN, WHICH
ORDER BY WHEN, WHICH
;
```

### simultaneous_persons_per_zone

```sql
WITH SUMS AS
(
SELECT TRUNC(WHEN) WHEN, WHICH, WHICH_2, SUM(HOW_MANY) TOTAL FROM ATOL_Z.T_ONSITE_STATISTICS WHERE WHAT='BY_ZONE'
GROUP BY WHEN, WHICH, WHICH_2
)
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || WHICH_2 || ',' || MAX(TOTAL)
 FROM SUMS GROUP BY WHEN, WHICH, WHICH_2
ORDER BY WHEN, WHICH, WHICH_2
;
```

### susi_distinct_persons_per_orgunit

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ',' || WHICH || ',' || CLASS || ',' || DEPARTMENT || ',' || "GROUP" || ',' || "COUNT"
  FROM ATOL_S.V_PRS_PER_DAY_ZONE_CLASS_GROUP
ORDER BY WHEN
;
```

### susi_simultaneous_persons_cern`

```sql
WITH SUMS AS
(
SELECT TRUNC(WHEN) WHEN, SUM(HOW_MANY) TOTAL
FROM ATOL_S.T_ONSITE_STATISTICS
WHERE WHAT='BY_ZONE'
GROUP BY WHEN
)
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ',' || MAX(TOTAL)
  FROM SUMS GROUP BY WHEN
ORDER BY WHEN
;
```

### susi_simultaneous_persons_per_zone

```sql
WITH SUMS AS
(
SELECT TRUNC(WHEN) WHEN, WHICH, SUM(HOW_MANY) TOTAL
FROM ATOL_S.T_ONSITE_STATISTICS
WHERE WHAT='BY_ZONE'
GROUP BY WHEN, WHICH
)
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ',' || WHICH || ',' || MAX(TOTAL)
  FROM SUMS GROUP BY WHEN, WHICH
ORDER BY WHEN, WHICH
;
```

### susi_transactions_per_acp

```sql
SELECT TO_CHAR(WHEN, 'YYYY-MM-DD') || ','|| WHICH || ',' || HOW_MANY
  FROM ATOL_S.T_ONSITE_STATISTICS
  WHERE WHAT='DAILY_MOVS_PER_ACP'
ORDER BY WHEN, WHICH;
```
