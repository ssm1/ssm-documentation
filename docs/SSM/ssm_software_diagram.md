# SSM Global Software Diagram

```mermaid
  graph TD;
      A[Zabbix Web Interface] --> B[Zabbix Server];
      B[Zabbix Server] --> C[Zabbix Proxy 1];
      B[Zabbix Server] --> D[Zabbix Proxy 2];
      B[Zabbix Server] --> E[Zabbix Proxy 3];
      B[Zabbix Server] --> F[Zabbix Proxy 4];
      C[Zabbix Proxy 1] & D[Zabbix Proxy 2] -----> G[Zabbix Agent Linux Host] & H[Zabbix Agent Linux Host];
      

      I[Grafana Web Interface] --> J[Grafana Server];
      J[Grafana Server] --> L[Local MySQL DB];
      J[Grafana Server] --> M[InfluxDB];
      J[Grafana Server] --> N[Zabbix DB];
      B[Zabbix Server] --> N[Zabbix DB];
      J[Grafana Server] --> O[DBOD MySQL DB];

      K[Oracle DB] --> P[SUSI Equipment];
      K[Oracle DB] --> Q[ZORA Equipment];
      K[Oracle DB] --> T[AdAMS];

      L[Local MySQL DB] --> R[SSM Scripts];
      O[DBOD MySQL DB] --> R[SSM Scripts];
      R[SSM Scripts] --> K[Oracle DB];

      M[InfluxDB] --> S[Telegraf];
      S[Telegraf] --> R[SSM Scripts]
      S[Telegraf] ----> U[SNMP Capable Devices]
      
      V[HealthChecks] <-----> R[SSM Scripts]

      W[SAM Web Server] -------> X[YCSAM devices]
      W[SAM Web Server] -------> Y[Y#AIP devices]
      

      click A "https://ssm-zabbix.cern.ch" _blank
      click I "https://ssm-grafana.cern.ch" _blank
      click V "https://ssm-hc.cern.ch" _blank
      click W "https://ssm.cern.ch/sam" _blank
```
