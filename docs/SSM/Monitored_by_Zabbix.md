# Being Monitored by SSM with Zabbix

With Zabbix Agent you get all the information about the host and you get alarms sent to egroups predefined. You can find the egroups here: <https://e-groups.cern.ch/e-groups/EgroupsSearch.do> doing a search of begins with: ```ssm-alert```

If you need to be monitored by SSM you need to install one of those systems depending of the needs and the host type.

## LINUX

Go to https://www.zabbix.com/download and choose your platform. In Zabbix version: 7.0 LTS and in Zabbix Component: Zabbix Agent 2.

Follow the instructions for the installation.


edit the file /etc/zabbix_agentd.conf and find the lines with

```bash
Server=127.0.0.1
ServerActive=127.0.0.1
Hostname=Zabbix server
```

Change by:

```bash
Server=ssm.cern.ch,ssm-dev.cern.ch,ssm-proxy-1.cern.ch,ssm-proxy-2.cern.ch,ssm-proxy-3.cern.ch,ssm-proxy-4.cern.ch,ssm-proxy-5.cern.ch,ssm-proxy-6.cern.ch,ssm-proxy-7.cern.ch,ssm-proxy-8.cern.ch,ssm-proxy-9.cern.ch
ServerActive=
Hostname=[your hostname].cern.ch
```

The rest of the configuration is standard for the Linux machines. You must to be open the firewall for the port 10050 and enable and start the service at reboot.

```bash
firewall-cmd --new-zone=zabbix --permanent
firewall-cmd --permanent --zone=zabbix --add-port=10050/tcp
firewall-cmd --reload
systemctl enable zabbix-agent && systemctl start zabbix-agent
```

Start the service and check it with:

```bash
systemctl start zabbix_agent2
systemctl status zabbix-agent2
```

## Windows

Install Zabbix Windows Agent by CMF (Preferred)
In order to have the correct agent and configuration, the best is adding the CERN NICE CMF package know as: Zabbix Agent X.Y.ZZ
The package can be find in the Named System Sets (NSS): ACCESS CONTROL-0. 
You should chose the latest version of the agent. In the moment of writting this documentation is the version **Zabbix Agent 7.0.6**

If there is not possible to install by CMF, you can install the agent manually with the next procedure.

Install Zabbix Windows Agent Manually

A complete and generic procedure with images is here:

<https://blog.masteringmdm.com/install-and-configure-zabbix-agent-windows/>

Delete Old Zabbix Service:
Just in case zabbix agent has been installed before. Run PowerShell as Administrator ->

```powershell
PS C:\ sc.exe delete "Zabbix Agent"
```

Download and install the agent

Go to <https://www.zabbix.com/download_agents>

Select Windows amd64 7.0 LTS No encryption (Verify that is Zabbix agent v7.0.X) and choose "Zabbix Agent 2" The direct link is: https://cdn.zabbix.com/zabbix/binaries/stable/7.0/7.0.6/zabbix_agent2-7.0.6-windows-amd64-static.zip 


In case you are in TN and cannot access external websites, you can download [HERE](zabbix_agent2-7.0.6-windows-amd64-static.zip) the last version for amd64.

Download the file and create a directory for Zabbix Agent and extract the zip there. I recommend c:\zabbix but can be whatever.

2 directories are created. conf and bin. The best is move all the exe and conf files to the main directory: C:\zabbix:

```powershell
PS C:\Zabbix> dir
 Directory: C:\Zabbix
 Mode                LastWriteTime         Length Name
 ----                -------------         ------ ----
 d-----       02/02/2021     10:42                bin
 d-----       02/02/2021     10:36                conf
 -a----       02/02/2021     10:05          12673 zabbix_agent2.conf
 -a----       02/02/2021     09:50         852992 zabbix_agent2.exe
 -a----       02/02/2021     09:41         252696 zabbix_agent2.log
 -a----       02/02/2021     09:50         186368 zabbix_get.exe
 -a----       02/02/2021     09:50         603648 zabbix_sender.exe
```

Edit with your text editor (notepad doesn’t edit correctly the file ): zabbix_agentd.conf and change:

```powershell
LogFile=c:\zabbix_agent2.log
```

Nothing wrong about having the log in the main disk but is more coherent to have:

```powershell
LogFile=c:\zabbix\zabbix_agent2.log
```

If the main directory for the install is: c:\zabbix

Change:

```powershell
Server=127.0.0.1
ServerActive=127.0.0.1
```

by:

```powershell
Server=ssm.cern.ch,ssm-dev.cern.ch,ssm-proxy-1.cern.ch,ssm-proxy-2.cern.ch,ssm-proxy-3.cern.ch,ssm-proxy-4.cern.ch,ssm-proxy-5.cern.ch,ssm-proxy-6.cern.ch,ssm-proxy-7.cern.ch,ssm-proxy-8.cern.ch,ssm-proxy-9.cern.ch
ServerActive=
```

Notepad doesn’t show the file zabbix_agentd.conf correctly
Notepad only recognizes CR, LF (0x0d, 0x0a), whereas other sources might use CR only, or LF only. You can’t make Notepad behave differently. Note that notepad is the only editor with this restriction, so better use something else.

One simple way to fix the line-feeds is to copy and paste the text into **Wordpad**, then back again into notepad, and the line-feeds will get “corrected” to the CR,LF sequence. Or edit directly in Wordpad saving as txt.

Prepare Windows firewall

By default, Zabbix agent communicates over 10050 port, and we’ll use the default port.

Add port TCP 10050 in the allow list for inbound communication. You could use GUI to achieve this, just open Windows Security, click on Firewall $ network protection and Allow an app through firewall , however, it is easier to run the following PowerShell command.

This will add TCP port 10050 in the inbound allow list. Run the following one-line command in PowerShell as administrator:

```powershell
New-NetFirewallRule -DisplayName "Allow inbound 10050" -Direction Inbound -Protocol TCP -Action Allow -LocalPort 10050 -Profile Any
```

Confirm the rule has been added by running the following command.

```powershell
Get-NetFirewallRule -DisplayName "Allow inbound 10050"
```

Install Zabbix as Service
Run the below command to install Zabbix:

```powershell
PS C:\zabbix> .\zabbix_agent2.exe -c .\zabbix_agent2.conf -i
```

You will see that the Zabbix Agent has been installed successfully.

You can start the agent in the Services of windows or in the command line with:

```powershell
PS C:\zabbix> .\zabbix_agent2.exe -s
```

If you edit the configuration file you must restart the agent in the Services of Windows or in the command line with:

```powershell
PS C:\zabbix> .\zabbix_agent2.exe -stop
PS C:\zabbix> .\zabbix_agent2.exe -start
```
