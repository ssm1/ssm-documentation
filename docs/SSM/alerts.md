# Alerting Zabbix

SSM can monitor the machines with several protocols or interfaces. For small devices the best approach is SNMP protocol that allows retrieve information about the network, disk, status, CPU, etc.

# Installation

If the host is a Windows or Linux machine, the best is use the agent that will collect the data from the host and/or send commands if needed and configured correctly.

For special devices, there are small scripts in the SSM core that can telnet, ftp or http the host in order to get the necessary values.

* Linux Agent Configuration
* Windows Agent Configuration
* SNMP Configuration

# Alerts

The alerts are sent by a SSM special address: alert@ssm.cern.ch. In general alarms are sent to an egroup. If you want to receive the alarms, you must subscribe to the egroup. The egroups configured are:

* ssm-alert-general. Alerts SSM for all systems
* ssm-alert-lhc SSM Monitoring Alerts SSM for LHC
* ssm-alert-sps Static SSM Monitoring Alerts SSM for SPS
* ssm-alert-susi Static SSM Monitoring Alerts SSM for SUSI
* ssm-alert-ps Static SSM Monitoring Alerts SSM for PS
* ssm-alert-ups Static SSM Monitoring Alerts SSM for UPS
* ssm-alert-db Static SSM Monitoring Alerts SSM for Databases

For special alerts, SMS, only one host, one alarm, etc. the alarms can be sent only to a user or group of users without going to the egroups.

For each problem 2 alerts are sent. One for when the problem start and one when the problem is finished. The alerts can be configured to only receive one email with the problem and not the recover

All the alarms start in the email with the subject:

PROBLEM: Trigger Description
RECOVER: Trigger Description

In the Body there is all the information about the problem and/or the recover.


# Template for alarms

[Problem] http://ssm.docs.cern.ch/