# SSM Statistics

The SSM statistics for the users and hosts web interfaces in the main server are here.

* [SAM Server](http://ssm.cern.ch/report.html)
* [SSM Zabbix User Interface](http://ssm-zabbix.cern.ch/report.html)
* [MySQL DBOD](https://dbod-mon.web.cern.ch/d/000000007/mysql-monitoring?var-server=dbod-ga049.cern.ch&var-instance=ssm&orgId=1)
* [InfluxDB DBOD](https://dbod-mon.web.cern.ch/d/000000004/influxdb-monitoring?var-server=dbod-ga039.cern.ch&var-instance=ssm_influxdb&orgId=1)
* [Grafana Internal](https://ssm-grafana.cern.ch/d/6HVBmh54k/grafana-internals?orgId=1)
