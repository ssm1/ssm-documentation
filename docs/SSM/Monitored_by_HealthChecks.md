# Being Monitored by SSM with HealthChecks

[SSM-HC](<https://ssm-hc.cern.ch>) is a service for monitoring cron jobs and similar periodic processes based in [Healthcheks](<https://healchecks.io>)

**ssm-hc.cern.ch** listens for HTTP requests ("pings") from your cron jobs and scheduled tasks.
It keeps silent as long as pings arrive on time.
It raises an alert as soon as a ping does not arrive on time.

[SSM-HC](<https://ssm-hc.cern.ch>) works as a dead man's switch for processes that need to run continuously or on a regular, known schedule. Some examples of jobs that would benefit from ssm-hc.cern.ch monitoring:

* filesystem backups, database backups
* task queues
* database replication status monitoring scripts
* report generation scripts
* periodic data import and sync jobs
* periodic antivirus scans
* DDNS updater scripts
* SSL renewal scripts

## Use in the SSM System

At the moment, more than 30 process are checked regularly with [SSM-HC](<https://ssm-hc.cern.ch>):

* Backups, checks and prune of backups for all servers.
* Getting data from Oracle to MySQL.
* Checking Database replication.

As example, here you can see some realtime checks for databases synchro between Oracle and MySQL:

<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/3sgJ5wWd/mysql.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/DW528iVL/oracle.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/L_qik9GN/ssm-grafana.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/r10AXcMw/susi.svg">
<img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/uyJ4llVa/zora.svg">

And a global badge grouping all the systems: <img src="https://ssm-hc.cern.ch/badge/2f64858e-0dc8-4440-895a-d83f82c03b21/K9W73CTC.svg">

## Concepts

A Check represents a single service you want to monitor. For example, when monitoring cron jobs, you would create a separate check for each cron job to be monitored. Each check has a unique ping URL, a set schedule, and associated integrations. For the available configuration options, see Configuring checks.

Each check is always in one of the following states, depicted by a status icon:

* New. A newly created check that has not received any pings yet. Each new check you create will start in this state.
* Up. All is well, the last "success" signal has arrived on time.
* Late. The "success" signal is due but has not arrived yet. It is not yet late by more than the check's configured Grace Time.
* Down. The "success" signal has not arrived yet, and the Grace Time has elapsed. When a check transitions into the "Down" state,

ssm-hc.cern.ch sends out alert messages via the configured integrations.
Paused. You can manually pause the monitoring of specific checks. For example, if a frequently running cron job has a known problem, and a fix is scheduled but not yet ready, you can pause monitoring of the corresponding check temporarily to avoid unwanted alerts about a known issue.
Additionally, if the most recent received signal is a "start" signal, this will be indicated by three animated dots under check's status icon.
Ping URL. Each check has a unique Ping URL. Clients (cron jobs, background workers, batch scripts, scheduled tasks, web services) make HTTP requests to the ping URL to signal a start of the execution, a success, or a failure.

ssm-hc.cern.ch supports two ping URL formats:

* <https://ssm-hc.cern.ch/>`<uuid>`
The check is identified by its UUID. Check UUIDs are assigned automatically by ssm-hc.cern.ch, and are guaranteed to be unique.

* <https://ssm-hc.cern.ch/>`<project-ping-key>/<name-slug>`
The check is identified by project's Ping key and check's slug (its name, converted to lowercase, spaces replaced with hyphens).
You can append /start, /fail or /<exitcode> to the base ping URL to send "start" and "failure" signals. The "start" and "failure" signals are optional. You don't have to use them, but you can gain additional monitoring insights if you do use them. See Measuring script run time and Signaling failures for details.

You should treat check UUIDs and project Ping keys as secrets. If you make them public, anybody can send telemetry signals to your checks and mess with your monitoring.

## Examples

### Oracle -> MySQL Bridge

For all the process running getting the data from Oracle to MySQL databases we must to check that all the scripts run correctly.

This is a script type:

```bash
#!/bin/bash

health=64369490-4900-46b7-a98f-da55e19040bf

curl -fsS -m 10 --retry 5 -o /dev/null  http://ssm-hc.cern.ch/ping/$health/start

####################
## Do the work and check a situation. The size of the file
####################

actual_lines=$(wc -l <"$file_csv")

if [ $actual_lines -ge $minimum_lines ]; then

  # The file is bigger than minimum_lines. In principle, good.
  # First, erase the data from the DB

  # Report to SSM-HC Healthchecks

  curl -fsS -m 10 --retry 5 -o /dev/null http://ssm-hc.cern.ch/ping/$health/$?

else

  # The file is empty or too small. Do nothing and arise the error
  echo "Error in file: $file_csv Probably a connection error"
  echo $output
  curl -fsS -m 10 --retry 5 --data-raw "$output" http://ssm-hc.cern.ch/ping/$health/fail

fi
```

In this case we have in ssm-hc.cern.ch

![Alt text](img/ssm-hc_check_screen.png)

All the script worked well. If something goes wrong, an alarm is sent to the manager configured:

![Alt text](img/example_error_ssm-hc.png)
