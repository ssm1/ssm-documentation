# Being Monitored by SSM with Grafana

All is based in a TIG stack that stand for Telegraf, InfluxDB, and Grafana.

InfluxDB is the Time Series Database in the TIG stack. InfluxDB is an open-source database optimized for fast, high-availability storage and retrieval of time series data written in Go. InfluxDB is great for operations monitoring, application metrics, and real-time analytics.

Telegraf is the agent for collecting and reporting metrics and data. Telegraf is part of the TIG Stack and is a plugin-driven server agent for collecting and reporting metrics. Telegraf has integrations to source a variety of metrics, events, and logs directly from the containers and systems it’s running on, pull metrics from third-party APIs, or even listen for metrics via StatsD and Kafka consumer services. It also has output plugins to send metrics to a variety of other data stores, services, and message queues, including InfluxDB, Graphite, OpenTSDB, Datadog, Librato, Kafka, MQTT, NSQ, and many others.

Grafana is an open-source metric analytics & visualization suite. It is most commonly used for visualizing time series data for infrastructure and application analytics but many use it in other domains including industrial sensors, home automation, weather, and process control. The tool provides a beautiful dashboard and metric analytics, with the ability to manage and create your own dashboard for your apps or infrastructure performance monitoring.

## SNMP

### Introduction

SNMP (Simple Network Management Protocol) is an application-layer protocol used to manage and monitor network devices. SNMP provides a common way for devices on your network — such as routers, switches, WiFi access points, printers, and anything connected in an IP network — to share monitoring metrics. An SNMP agent is shipped with practically any network equipment, providing an out-of-the-box way to monitor devices over both local area networks (LANs) and wide area networks (WANs). SNMP has also been adopted by some internet of things (IoT) devices vendors.

In typical SNMP uses, one or more administrative computers, called managers (SSM), have the task of monitoring or managing a group of hosts or devices on a computer network. Each managed system executes, at all times, a software component called an agent which reports information via SNMP to the manager.

Essentially, SNMP agents expose management data on the managed systems as variables. The protocol also permits active management tasks, such as modifying and applying a new configuration through remote modification of these variables. The variables accessible via SNMP are organized in hierarchies. These hierarchies, and other metadata (such as type and description of the variable), are described by Management Information Bases (MIBs).

### Network and system monitoring solution with Telegraf SNMP and Telegraf Input Plugin

Telegraf SNMP plugin provide a comprehensive solution for monitoring SNMP data from networked devices. Device metrics resource utilization, load status and health check as well as network performance metrics can be monitored via SNMP monitoring.

Telegraf is the collection agent of InfluxData time series platform (see diagram below). InfluxData time series platform allows for the collection, visualization and analysis of all metrics and events ingested and monitored. InfluxDB — the purpose-built time series database is the platform’s core open source component. Telegraf is InfluxDB’s agent with hundreds of plugins for the most common applications and protocols ready to be activated and to start collecting metrics and events. Telegraf is also open source with its own project in the community.

One of the main use-cases for adopting a time series platform is systems and network monitoring. Gathering the data of interest is a key part of any monitoring solution. This can be done in multiple ways.

To facilitate the process, Telegraf was designed as a lightweight, plugin-driven collection agent that can either run on your hosts (collecting data about your systems and applications) or operate remotely (scraping data via endpoints exposed by your applications).

See below TICK Stack architecture:

![Alt text](img/snmp-integration-influxdata-architecture.png)

### Configuration

The Telegraf SNMP input plugin uses polling to gather metrics from SNMP agents. The plugin also includes support for gathering individual OIDs as well as complete SNMP tables.

The device must have SNMP protocol configured and active.

!!! Warning
    The “Community Read” name must be: ```public``` Not ```Public```, or ```PUBLIC``` or ```PuBlIc```!!!

Be sure that the firewall (if any) allow the communications on the port TCP: 161
The Telegraf SNMP input plugin uses polling to gather metrics from SNMP agents. The plugin also includes support for gathering individual OIDs as well as complete SNMP tables.

An example of telegraf config for 2 UPS's is:

```bash

[[inputs.snmp]]
  agents = [
"YZUPS01-212",
"YZUPS01-BA3",
"YZUPS02-SZ33" ]
  version = 1
  community = "public"
  interval = "300s"
  timeout = "5s"
  retries = 3
  max_repetitions = 100
  name = "ups_benning_lhc"
  [[inputs.snmp.field]]
    name = "Temperature Battery 1"
    oid = ".1.3.6.1.4.1.27383.1.1.5.3.1.5.1"
  [[inputs.snmp.field]]
    name = "Temperature Battery 2"
    oid = ".1.3.6.1.4.1.27383.1.1.5.3.1.5.2"

```

### Hardware Supported by SSM via Grafana

* Mobotix Cameras
* Vivotek Cameras
* NKF
* UPS Benning
* UPS Riello
* HP Proliants
* S7 Siemens PLC
