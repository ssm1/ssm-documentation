# Links

* [GRAFANA](<https://ssm-grafana.cern.ch/>){target="_blank"}
* [ZABBIX](<https://ssm-zabbix.cern.ch/index.php?enter=guest>){target="_blank"} (There is not login/password for the application but you must to have a SSO CERN login. If you arrive directly to ssm-zabbix.cern.ch just click on [**sign in as guest**](https://ssm-zabbix.cern.ch/index.php?enter=guest))
* [CSAM Dashboards](<https://ssm-grafana.cern.ch/d/VGP_t4umz/windows-server-status>){target="_blank"}
* [CSAM General Status](<https://ssm-grafana.cern.ch/d/cReG24uiz/csam-global-status?refresh=5s&orgId=1&var-group=CSAM&var-host=All>){target="_blank"}
* [SPS](<https://ssm-grafana.cern.ch/d/VGP_t4umz/windows-server-status>){target="_blank"}
* [SUSI](<https://ssm-grafana.cern.ch/dashboards/f/_S2WCnvmk/access-susi>){target="_blank"} 


## New development

* [GRAFANA2](<https://ssm-grafana2.cern.ch/>){target="_blank"} New development in Openstack Cluster and kubernetes.
* [ZABBIX2](<https://ssm-zabbix2.cern.ch/index.php?enter=guest>){target="_blank"} New development in Openstack Cluster and kubernetes. Stopped for the moment.
* [ZABBIX7](<https://ssm-zabbix.cern.ch/index.php?enter=guest>){target="_blank"} New Zabbix version 7 in new OS AlmaLinux 9. Already in production 

<!--- 
* [SAM](<http://ssm.cern.ch/sam>){target="_blank"} 
* [SAM 2.0](<http://ssm-sam.cern.ch>){target="_blank"} (For the moment, the "old"  SAM is syncronized to the new one) 
--->

