<!---
your comment goes here
and here
# NEW Page for SSM

I'm exciting to show you the new pages for SSM Documentation.
The change has been motivated for several reasons:

* New Year 2023 ;-)
* We had a big Wordpress and was too much for the information we had, also Wordpress is not well supported at CERN.
* Version follow up in gitlab.
* Possibility of preview the site locally before sending the modifications.
* Use of flat text files format in markdown. Easy to migrate or edit.

!!!note
    You can find all the old links [HERE](links.md) and the long time showing image for the site is in the bottom.

[Your comments are very welcome](<mailto:tono.riesco@cern.ch>).
-->
## Overview

SSM stands for Safety Systems Monitoring and the main goal is undertake all the activities and processes to assure that the systems are operating according to expectations and requirements defined.

Safety performance monitoring and measurement represents the means to verify the safety performance of the systems supervised by SSM and to validate the effectiveness of safety equipments.

SSM must provide the Operational Safety Systems with diagnostic data, so that they can take corrective action in case of incident. Preferably before it becomes apparent to the operations teams or the users of the system.

SSM must provide the Operational Safety Systems team with long term diagnostic data, so that post-mortem incident analysis can be made, usage statistics can be extracted and so that trends can be analyzed for future design considerations.

In a more general approach, the SSM system must assure that the Safety Status of the Operational System is maintained and if not, provide the necessary information to be repaired as soon as possible. This will allow to effectively reduce the risk from an “unacceptable” to an “acceptable” level.

## General Documentation

Documentation for SSM **[HERE](SSM/ssm_report.md)**

<!---Documentation for SAM **[HERE](SAM/sam_documentation.md)**--->

![Old Image](SSM/img/ssm_old_image.jpg)
