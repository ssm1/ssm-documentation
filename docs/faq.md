# FAQ

## I see "Not trusted certificate" when connecting to SSM servers

If you get the certificate error or in the browser bar you don’t have the “green” padlock, probably is because you don’t have the CERN certificates. The CERN IT force to use its own certificates that is not in the standard browsers.

You can download and install [HERE](<https://cafiles.cern.ch/cafiles/certificates/Cern.aspx>){target="_blank"}

## I need supervision for X with Zabbix, Grafana, HealthChecks what I have to do?

[Contact me](mailto:tono.riesco@cern.ch) .  We must check together what do you need and what is the best solution to achieve it. There is not automatic configuration in SSM.

## How can I clone a linux system?

Look [HERE](Util/full_system_backup_restore_cloning.md)

## Why I cannot see some dashboards, panels or information in SSM Services?

There is certain information that cannot be shared freely. If you need to access the data, you need to have a valid CERN account and be approved by Access Management to have access. If you think you should see any information, please send an email to <access.control.support@cern.ch>

## How I clone a influxdb database without access to the second server

Look [HERE](Util/clone_influxdb.md)

## How do I create an alert in SSM Grafana

Look [HERE](https://grafana.com/tutorials/alerting-get-started/)
